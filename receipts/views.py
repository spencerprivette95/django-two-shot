from django.shortcuts import render  # #get_object_or_404, get_list_or_404
from receipts.models import Receipt, Account, ExpenseCategory


# Create your views here.
def receipts_list(request):
    receipts_list = Receipt.objects.all()
    context = {
        "receipts_list": receipts_list,
    }
    return render(request, "receipts/home.html", context)
